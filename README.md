<!--
SPDX-FileCopyrightText: 2021 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: Apache-2.0
-->

Checkmk-Agent Role
==================

This Ansible role installs the checkmk-agent.

Requirements
------------

None.

Role Variables
--------------

```yaml
checkmk_version: "2.2.0p24-1.b3e7bae82504c1d2"
```
Version of the CheckMK debian package to be installed

```yaml
checkmk_download_url: "https://syncandshare.desy.de/index.php/s/8cA5ibrE25HD47E/download/check-mk-agent_2.2.0p24-b3e7bae82504c1d2_all.deb"
```
URL to download the CheckMK package from

```yaml
checkmk_checksum: "sha256:0b828f961e68b8fe207d3698797799798d761bb3f042c47c1189a38df74db2c8"
```
Checksum of the downloaded debian package

```yaml
checkmk_install_nginx_plugin: false
```
Decide wether to install the NGINX plugin

```yaml
checkmk_nginx_plugin_url: "https://raw.githubusercontent.com/Checkmk/checkmk/refs/tags/v{{ checkmk_version | split('-') | first }}/agents/plugins/nginx_status.py"
```
URL targeting the nginx_status plugin

```yaml
checkmk_nginx_plugin_checksum: "sha256:73efad0cb070c16f3c3d7e1a14b8f74f84e3c76383c4a8562e04cc80b36b3cc1"
```
Checksum of the NGINX plugin

```yaml
checkmk_nginx_plugin_enable_autodetect: True
```
Decide whether the NGINX plugin should use autodetect or not

```yaml
checkmk_nginx_plugin_port: 80
```
Port of the NGINX server which the plugin should listen to

```yaml
checkmk_nginx_plugin_address: "localhost"
```
Address of the NGINX server which the plugin should listen to

```yaml
checkmk_nginx_plugin_protocol: "http"
```
Protocol of the NGINX server which the plugin should use.
This can either be `http` or `https`.

```yaml
checkmk_nginx_plugin_cfg_template: "nginx_status.cfg.j2"
```
Configuration template of the NGINX plugin

Dependencies
------------

None.

License
-------

[Apache-2.0](LICENSES/Apache-2.0.txt)

Author Information
------------------

This role is maintained and created by HIFIS Software.
