<!--
SPDX-FileCopyrightText: 2021 Helmholtz Centre for Environmental Research (UFZ)
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: Apache-2.0
-->

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Group your changes into these categories:

`Added`, `Changed`, `Deprecated`, `Removed`, `Fixed`, `Security`.

## [0.5.0](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/releases/v0.4.0) - 2023-08-18

[List of commits](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/compare/v0.4.0...v0.5.0)

### Changed

* chore: install checkmk-agent 2.1.0p32-1
  ([!95](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/merge_requests/95)
  by [tobiashuste](https://codebase.helmholtz.cloud/frust45)).
* Dependency updates

## [0.4.0](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/releases/v0.4.0) - 2022-12-19

[List of commits](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/compare/v0.3.0...v0.4.0)

### Changed

* Update Podman to version 4.3.1 in GitLab CI
  ([!42](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/merge_requests/42)
  by [tobiashuste](https://codebase.helmholtz.cloud/frust45)).
* Install checkmk agent version 2.1.0p17 by default
  ([!41](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/merge_requests/41)
  by [tobiashuste](https://codebase.helmholtz.cloud/frust45)).

## [0.3.0](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/releases/v0.3.0) - 2022-07-18

[List of commits](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/compare/v0.2.1...v0.3.0)

### Changed

* Improve CI tests and install the most recent CheckMK agent
  ([!7](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/merge_requests/7)
  by [tobiashuste](https://codebase.helmholtz.cloud/frust45)).

## [0.2.1](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/releases/v0.2.1) - 2021-06-03

[List of commits](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/compare/v0.2.0...v0.2.1)

### Fixed

* Fix exception when running the nginx agent plugin with Python 3.8
  ([!6](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/merge_requests/6)
  by [tobiashuste](https://codebase.helmholtz.cloud/frust45)).

## [0.2.0](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/releases/v0.2.0) - 2021-06-03

[List of commits](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/compare/v0.1.0...v0.2.0)

### Added

* Allow to install and configure the NGINX agent plugin
  ([!4](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/merge_requests/4)
  by [tobiashuste](https://codebase.helmholtz.cloud/frust45)).

## [0.1.0](https://codebase.helmholtz.cloud/hifis-software-deployment/checkmk-role/-/releases/v0.1.0) - 2021-05-27

### Added

Initial release of the Ansible CheckMK Role.
